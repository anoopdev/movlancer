package com.app.movlancer.data

import android.support.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Movie(
    @SerializedName("id") val id: String,
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("adult") val isAdult: Boolean,
    @SerializedName("overview") val overview: String,
    @SerializedName("release_date") val releaseDate: String,
    @SerializedName("genre_ids") val genreIds: List<Int>,
    @SerializedName("original_title") val originalTitle: String,
    @SerializedName("original_language") val originalLanguage: String,
    @SerializedName("backdrop_path") val backdropPath: String?,
    @SerializedName("popularity") val popularity: Float,
    @SerializedName("vote_count") val voteCount: Int,
    @SerializedName("video") val isVideo: Boolean,
    @SerializedName("title") val title: String,
    @SerializedName("vote_average") val voteAverage: Float
)

@Keep
data class PopularMovies(
    @SerializedName("page") val page: Int = 0,
    @SerializedName("results") val popularMovieList: List<Movie> = listOf(),
    @SerializedName("total_results") val totalResults: Int = 0,
    @SerializedName("total_pages") val totalPages: Int = 0
)