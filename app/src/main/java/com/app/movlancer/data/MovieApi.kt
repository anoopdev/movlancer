package com.app.movlancer.data

import com.app.movlancer.BuildConfig
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieRetrofitApi {
    @GET("movie/popular?api_key=${BuildConfig.API_KEY}")
    fun getPopularMovies(@Query("page") page: Int): Single<PopularMovies>
}

class MovieApi(private val movieRetrofitApi: MovieRetrofitApi) {
    fun getPopularMovies(page: Int): Single<PopularMovies> {
        return movieRetrofitApi.getPopularMovies(page)
    }
}