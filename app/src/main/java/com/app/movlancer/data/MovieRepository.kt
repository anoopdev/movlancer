package com.app.movlancer.data

import io.reactivex.Single

class MovieRepository(private val movieApi: MovieApi) {

    fun getPopularMovies(page: Int): Single<PopularMovies> {
        return movieApi.getPopularMovies(page)
    }
}