package com.app.movlancer.di

import com.app.movlancer.data.MovieApi
import com.app.movlancer.data.MovieRepository
import com.app.movlancer.data.MovieRetrofitApi
import com.app.movlancer.ui.movies.viewmodel.MovieViewModelProviderFactory
import com.app.movlancer.util.NetworkUtil
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class MovieModule {

    @Singleton
    @Provides
    fun provideMovieRetrofitApi(retrofit: Retrofit): MovieRetrofitApi {
        return retrofit.create(MovieRetrofitApi::class.java)
    }

    @Singleton
    @Provides
    fun providesMovieApi(movieRetrofitApi: MovieRetrofitApi): MovieApi {
        return MovieApi(movieRetrofitApi)
    }

    @Singleton
    @Provides
    fun providesMovieRepository(movieApi: MovieApi): MovieRepository {
        return MovieRepository(movieApi)
    }

    @Singleton
    @Provides
    fun providesMovieViewModelProviderFactory(
        movieRepository: MovieRepository,
        networkUtil: NetworkUtil
    ): MovieViewModelProviderFactory {
        return MovieViewModelProviderFactory(movieRepository, networkUtil)
    }
}