package com.app.movlancer.di

import android.content.Context
import com.app.movlancer.util.NetworkUtil
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideNetworkUtil(): NetworkUtil {
        return NetworkUtil(context)
    }
}