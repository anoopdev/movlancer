package com.app.movlancer.di

import com.app.movlancer.ui.movies.view.PopularMoviesActivity
import com.app.movlancer.MovLancer
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, OkHttpModule::class, RetrofitModule::class, MovieModule::class])
interface AppComponents {
    fun inject(movLancer: MovLancer)
    fun inject(popularMoviesActivity: PopularMoviesActivity)
}