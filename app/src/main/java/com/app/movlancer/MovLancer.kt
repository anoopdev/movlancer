package com.app.movlancer

import android.app.Application
import android.content.Context
import com.app.movlancer.di.*

class MovLancer : Application() {

    var instance: Context = this

    companion object {
        lateinit var appComponent: AppComponents
    }

    init {
        createAppComponent()
    }

    private fun createAppComponent() {
        appComponent = DaggerAppComponents.builder()
            .appModule(AppModule(instance))
            .okHttpModule(OkHttpModule())
            .retrofitModule(RetrofitModule())
            .movieModule(MovieModule())
            .build()
        appComponent.inject(this)
    }
}