package com.app.movlancer.ui.movies.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.app.movlancer.data.MovieRepository
import com.app.movlancer.util.NetworkUtil

/**
 *  Creates [MovieViewModel] with [MovieRepository] as [NetworkUtil] as constructor argument
 */
class MovieViewModelProviderFactory(
    private val movieRepository: MovieRepository,
    private val networkUtil: NetworkUtil
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MovieViewModel::class.java)) {
            MovieViewModel(movieRepository, networkUtil) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}