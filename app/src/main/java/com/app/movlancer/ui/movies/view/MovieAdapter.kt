package com.app.movlancer.ui.movies.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.app.movlancer.data.Movie

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    var movieList: MutableList<Movie> = mutableListOf()
        set(value) {
            // This can be useful when implementing the search api.
            // It will be new results every time
            field = value
            notifyDataSetChanged()
        }

    fun appendMovies(movies: List<Movie>) {
        // Find the last position of the list for
        // notifying the adapter after appending new list
        val lastPosition = movieList.size
        movieList.addAll(movies)
        notifyItemInserted(lastPosition)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(MovieGridItemView(parent.context))
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        (holder.view as MovieGridItemView).bind(movieList[position])
    }

    data class MovieViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}