package com.app.movlancer.ui.movies.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.Toolbar
import android.view.View
import com.app.movlancer.MovLancer
import com.app.movlancer.R
import com.app.movlancer.extentions.isLandscapeMode
import com.app.movlancer.extentions.subscribeAndObserveOnMainThread
import com.app.movlancer.ui.movies.viewmodel.MovieViewModel
import com.app.movlancer.ui.movies.viewmodel.MovieViewModelProviderFactory
import kotlinx.android.synthetic.main.popular_movies_activity_layout.*
import javax.inject.Inject

const val KEY_RECYCLER_VIEW_STATE = "keyRecyclerViewState"

class PopularMoviesActivity : AppCompatActivity() {

    @Inject
    lateinit var movieViewModelProviderFactory: MovieViewModelProviderFactory

    private val movieViewModel: MovieViewModel by lazy {
        ViewModelProviders.of(this, movieViewModelProviderFactory).get(MovieViewModel::class.java)
    }

    private var movieListState: Parcelable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.popular_movies_activity_layout)
        MovLancer.appComponent.inject(this)
        initView()
    }

    private fun initView() {
        val toolbar = findViewById<Toolbar>(R.id.appBar)
        toolbar.inflateMenu(R.menu.movie_list_menu)

        val movieAdapter = MovieAdapter()
        val spanCount = if (isLandscapeMode()) GridSpanCount.Landscape.value else GridSpanCount.Portrait.value
        val layoutManager = GridLayoutManager(this, spanCount)
        movieListView.adapter = movieAdapter
        movieListView.layoutManager = layoutManager

        movieListView.moreItemLoads.subscribeAndObserveOnMainThread {
            movieViewModel.loadMoreItems()
        }
        movieViewModel.errorMessageShows.subscribeAndObserveOnMainThread { errorMessageStringRes ->
            Snackbar.make(movieListView, getString(errorMessageStringRes), Snackbar.LENGTH_SHORT).show()
        }
        movieViewModel.popularMoviesListLoads.subscribeAndObserveOnMainThread { movieList ->
            movieAdapter.appendMovies(movieList)
            // This is to ensure the recycler view position remains in same after the orientation change
            movieListState?.let { state ->
                movieListView.layoutManager.onRestoreInstanceState(state)
                // Need to set it back to null after restoring the recycler view position
                // so that on next subscription this wont fire again
                movieListState = null
            }

        }
        movieViewModel.progressViewVisibilityChanges.subscribeAndObserveOnMainThread { isVisible ->
            progressBar.visibility = if (isVisible) View.VISIBLE else View.GONE
        }

        movieViewModel.itemEnds.subscribeAndObserveOnMainThread { isLoadMore ->
            // If the actual list ends, this will ensure the not to call load more items
            movieListView.loadMoreItems = isLoadMore
        }

        movieViewModel.loadPopularMovies()
    }

    enum class GridSpanCount(val value: Int) {
        Portrait(2), Landscape(3)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Saving the movie list state on screen rotation
        outState?.putParcelable(KEY_RECYCLER_VIEW_STATE, movieListView.layoutManager.onSaveInstanceState())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        // Restore the list state after screen rotation
        savedInstanceState?.let {
            movieListState = it.getParcelable(KEY_RECYCLER_VIEW_STATE)
        }
    }
}
