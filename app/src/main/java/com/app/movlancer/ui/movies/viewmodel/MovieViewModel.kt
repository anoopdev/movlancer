package com.app.movlancer.ui.movies.viewmodel

import android.arch.lifecycle.ViewModel
import com.app.movlancer.R
import com.app.movlancer.data.Movie
import com.app.movlancer.data.MovieRepository
import com.app.movlancer.util.NetworkUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MovieViewModel(
    private val movieRepository: MovieRepository,
    private val networkUtil: NetworkUtil
) : ViewModel() {

    private val popularMoviesListLoadsSubject = PublishSubject.create<List<Movie>>()
    val popularMoviesListLoads: Observable<List<Movie>> = popularMoviesListLoadsSubject.hide()

    private val progressViewVisibilityChangesSubject = PublishSubject.create<Boolean>()
    val progressViewVisibilityChanges: Observable<Boolean> = progressViewVisibilityChangesSubject.hide()

    private val errorMessageShowsSubject = PublishSubject.create<Int>()
    val errorMessageShows: Observable<Int> = errorMessageShowsSubject.hide().throttleFirst(500, TimeUnit.MILLISECONDS)

    private val itemEndsSubject = PublishSubject.create<Boolean>()
    val itemEnds: Observable<Boolean> = itemEndsSubject.hide()

    private val compositeDisposable = CompositeDisposable()
    private var currentPageNumber = 1
    private var totalPages = 1
    private var popularMovies: MutableList<Movie> = mutableListOf()

    fun loadPopularMovies() {
        progressViewVisibilityChangesSubject.onNext(true)
        if (popularMovies.isNotEmpty()) {
            progressViewVisibilityChangesSubject.onNext(false)
            popularMoviesListLoadsSubject.onNext(popularMovies)
        } else {
            fetchPopularMoviesForPageFromApi(currentPageNumber)
        }
    }

    private fun fetchPopularMoviesForPageFromApi(pageNumber: Int) {
        if (!networkUtil.isNetworkAvailable()) {
            errorMessageShowsSubject.onNext(R.string.please_check_the_internet_connection)
            progressViewVisibilityChangesSubject.onNext(false)
            return
        }

        compositeDisposable.add(
            movieRepository.getPopularMovies(pageNumber)
                .doAfterTerminate { progressViewVisibilityChangesSubject.onNext(false) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ popularMoviesResponse ->
                    currentPageNumber = popularMoviesResponse.page
                    totalPages = popularMoviesResponse.totalPages

                    popularMoviesListLoadsSubject.onNext(popularMoviesResponse.popularMovieList)
                    // We need to keep the movies list locally so that if the user rotates
                    // the devices we don't need to fetch all the list again
                    popularMovies.addAll(popularMoviesResponse.popularMovieList)

                }, {
                    Timber.e(it, "Loading popular movies failed")
                    errorMessageShowsSubject.onNext(R.string.failed_to_load_movies)
                })
        )
    }

    fun loadMoreItems() {
        if (totalPages == currentPageNumber) {
            // if the total page and current page number is same
            // we don't need to make another network call.
            // And no need to track the recycler view scrolling
            itemEndsSubject.onNext(true)
            return
        }
        progressViewVisibilityChangesSubject.onNext(true)
        fetchPopularMoviesForPageFromApi(currentPageNumber + 1)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}