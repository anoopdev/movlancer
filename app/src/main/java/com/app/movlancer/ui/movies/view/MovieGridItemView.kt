package com.app.movlancer.ui.movies.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.view.View
import com.app.movlancer.R
import com.app.movlancer.data.Movie
import com.app.movlancer.extentions.convertTenRatingValueToFiveRatingValue
import com.app.movlancer.extentions.getPosterUrl
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.movie_grid_item_view.view.*

const val ROUND_CORNER_IMAGE_RADIUS = 20

class MovieGridItemView(context: Context) : ConstraintLayout(context) {

    init {
        layoutParams = ConstraintLayout
            .LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        View.inflate(context, R.layout.movie_grid_item_view, this)
    }

    fun bind(movie: Movie) {
        val roundCornerRequestOption = RequestOptions().transforms(
            CenterCrop(), RoundedCorners(
                ROUND_CORNER_IMAGE_RADIUS
            )
        )
        Glide.with(context)
            .load(movie.getPosterUrl())
            .apply(roundCornerRequestOption)
            .into(moviePoster)
        movieTitle.text = movie.title
        movieRating.rating = movie.voteAverage.convertTenRatingValueToFiveRatingValue()
    }
}