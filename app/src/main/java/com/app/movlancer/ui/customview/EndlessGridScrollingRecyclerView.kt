package com.app.movlancer.ui.customview

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

const val LOAD_NEXT_SET_OFFSET = 5

class EndlessGridScrollingRecyclerView : RecyclerView {

    private val moreItemLoadsSubject = PublishSubject.create<Unit>()
    val moreItemLoads: Observable<Unit> = moreItemLoadsSubject.hide().throttleFirst(500, TimeUnit.MILLISECONDS)
    var loadMoreItems = true

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = (layoutManager as GridLayoutManager).findFirstVisibleItemPosition()

                // Setting limit of 5 before reaching the end of the
                // list so that we can load the items and get a endless scrolling effect.
                if (loadMoreItems && ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount - LOAD_NEXT_SET_OFFSET)) {
                    moreItemLoadsSubject.onNext(Unit)
                }
            }
        })
    }

}