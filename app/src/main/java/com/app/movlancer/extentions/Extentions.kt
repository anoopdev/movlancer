package com.app.movlancer.extentions

import android.app.Activity
import android.util.DisplayMetrics
import com.app.movlancer.data.Movie
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

const val MOVIE_POSTER_BASE_URL = "https://image.tmdb.org/t/p/w500"

fun Movie.getPosterUrl(): String {
    return "$MOVIE_POSTER_BASE_URL$posterPath"
}

fun <T> Observable<T>.subscribeAndObserveOnMainThread(onNext: (t: T) -> Unit): Disposable {
    return observeOn(AndroidSchedulers.mainThread())
        .subscribe(onNext)
}

fun Activity.isLandscapeMode(): Boolean {
    val displayMetrics = DisplayMetrics()
    this.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return (displayMetrics.widthPixels > displayMetrics.heightPixels)
}

fun Float.convertTenRatingValueToFiveRatingValue(): Float {
    return (this * 0.5).toFloat()
}