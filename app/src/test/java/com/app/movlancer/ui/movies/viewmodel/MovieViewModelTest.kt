package com.app.movlancer.ui.movies.viewmodel

import com.app.movlancer.ImmediateSchedulersRule
import com.app.movlancer.R
import com.app.movlancer.data.Movie
import com.app.movlancer.data.MovieRepository
import com.app.movlancer.data.PopularMovies
import com.app.movlancer.util.NetworkUtil
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MovieViewModelTest {

    @get: Rule
    val immediateSchedulersRule = ImmediateSchedulersRule()

    private lateinit var moveViewModelUnderTest: MovieViewModel
    private val mockMovieRepository: MovieRepository = mock()
    private val mockNetworkUtil: NetworkUtil = mock()
    private val mockPopularMovies: PopularMovies = mock()
    private val mockMovie: Movie = mock()
    private lateinit var loadPopularMovieTestObserver: TestObserver<List<Movie>>
    private lateinit var progressViewChangesTestObserver: TestObserver<Boolean>
    private lateinit var errorMessageShowsTestObserver: TestObserver<Int>

    private val pageNumber = 1

    @Before
    fun setUp() {
        moveViewModelUnderTest = MovieViewModel(mockMovieRepository, mockNetworkUtil)
        loadPopularMovieTestObserver = moveViewModelUnderTest.popularMoviesListLoads.test()
        progressViewChangesTestObserver = moveViewModelUnderTest.progressViewVisibilityChanges.test()
        errorMessageShowsTestObserver = moveViewModelUnderTest.errorMessageShows.test()
    }

    @Test
    fun `popular movies loads successfully`() {
        // Given
        whenever(mockNetworkUtil.isNetworkAvailable())
            .thenReturn(true)
        whenever(mockMovieRepository.getPopularMovies(pageNumber))
            .thenReturn(Single.just(mockPopularMovies))
        whenever(mockPopularMovies.popularMovieList)
            .thenReturn(listOf(mockMovie))
        whenever(mockPopularMovies.page)
            .thenReturn(1)
        whenever(mockPopularMovies.totalPages)
            .thenReturn(1)
        whenever(mockPopularMovies.totalResults)
            .thenReturn(1)

        // When
        moveViewModelUnderTest.loadPopularMovies()

        // Then
        progressViewChangesTestObserver.assertValueSet(listOf(true, false))
        loadPopularMovieTestObserver.assertValue(listOf(mockMovie))

    }

    @Test
    fun `loading popular movies failed`() {
        // Given
        whenever(mockNetworkUtil.isNetworkAvailable())
            .thenReturn(true)
        whenever(mockMovieRepository.getPopularMovies(pageNumber))
            .thenReturn(Single.error(Throwable("Failed to load the popular movies")))

        // When
        moveViewModelUnderTest.loadPopularMovies()

        // Then
        progressViewChangesTestObserver.assertValueSet(listOf(true, false))
        errorMessageShowsTestObserver.assertValue(R.string.failed_to_load_movies)
    }

    @Test
    fun `loading popular movies failed because of network error`() {
        // Given
        whenever(mockNetworkUtil.isNetworkAvailable())
            .thenReturn(false)

        // When
        moveViewModelUnderTest.loadPopularMovies()

        // Then
        progressViewChangesTestObserver.assertValueSet(listOf(true, false))
        errorMessageShowsTestObserver.assertValue(R.string.please_check_the_internet_connection)
    }
}